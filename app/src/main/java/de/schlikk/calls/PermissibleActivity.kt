package de.schlikk.calls

import android.app.NotificationManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.provider.Settings
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

open class PermissibleActivity : AppCompatActivity() {
    private var activeCall: PermissionCall? = null
    
    
    
    private fun requestPermissionsAndCall( call: PermissionCall ) {
        Log.d( "PermissibleActivity", "register permissions request" )
        if( activeCall != null ) {
            Log.w( "PermissibleActivity", "requestPermissionsAndCall overrides $activeCall with $call" )
        }
        activeCall = call
        Log.d( "PermissibleActivity", "request permissions" )
        ActivityCompat.requestPermissions( this, arrayOf( call.permission ), call.requestCode )
    }
    
    fun callUnderPermission( call: PermissionCall ) {
        Log.d( "PermissibleActivity", "checkSelfPermission: ${call.permission}" )
        if( ActivityCompat.checkSelfPermission( this, call.permission ) != PackageManager.PERMISSION_GRANTED ) {
            Log.d( "PermissibleActivity", "- not granted => requestPermission" )
            requestPermissionsAndCall( call )
            return
        }
        Log.d( "PermissibleActivity", "all permissions granted => execute given runnable" )
        call.grantAction?.invoke()
    }
    
    
    
    override fun onRequestPermissionsResult( requestCode: Int, permissions: Array<out String>, grantResults: IntArray ) {
        val call = activeCall
        if( call == null ) {
            Log.w( "PermissibleActivity", "there's no call registered for onRequestPermissionsResult("
                    + " requestCode=$requestCode"
                    + ", permission=" + permissions.contentToString()
                    + ", ... )" )
            return
        }
        
        if( grantResults.isNotEmpty() ) {
            if( grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                call.grantAction?.invoke()
                activeCall = null
            }
            else {
                if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && shouldShowRequestPermissionRationale( call.permission ) ) {
                    Log.d( "PermissibleActivity", "- buildVersion >= M && shouldShowRequestPermissionRationale => show educational dialog" )
                    // https://developer.android.com/training/permissions/requesting#java
    
                    val okayMessage = call.positiveActionButton?.first ?: getString( R.string.generalOkay )
                    
                    val builder = AlertDialog.Builder( this )
                        .setMessage( call.denyDialogMessage )
                        .setPositiveButton( okayMessage ) { _, _ ->
                            Log.d("PermissibleActivity", "- => okay")
                            call.positiveActionButton?.second?.invoke()
                        }
                    
                    if( call.negativeActionButton != null ) {
                        builder.setNegativeButton( call.negativeActionButton!!.first ) { _, _ ->
                            Log.d("PermissibleActivity", "- => no thanks")
                            call.negativeActionButton!!.second()
                        }
                    }
                    builder
                        .create()
                        .show()
                }
                else if( call.neverAskAgainAction != null ) {
                    call.neverAskAgainAction!!.invoke()
                }
            }
        }
    }
    
    

    fun requireNotificationPolicyAccess(): Boolean {
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            val notificationManager = getSystemService( NOTIFICATION_SERVICE ) as NotificationManager
            if( !notificationManager.isNotificationPolicyAccessGranted ) {
                // permission to change volume during DoNotDisturb ( NotificationPolicyAccess )
                //     has to be granted in a special android dialog outside the app
                // see also https://stackoverflow.com/questions/39151453/in-android-7-api-level-24-my-app-is-not-allowed-to-mute-phone-set-ringer-mode
                startActivity( Intent( Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS ) )
                return false
            }
        }
        return true
    }
    
    
    
    fun closeActivityBecauseOfPermissionDenial() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity()
        }
        else {
            finish()
        }
    }
}
