package de.schlikk.calls

import android.content.Context
import java.text.SimpleDateFormat
import java.util.*

class PreferenceBasedLogger( context: Context, val storeKey: String ) {
    val sharedPreferences = context.getSharedPreferences(null, Context.MODE_PRIVATE)
    
    var lastLogMillis = 0L
    var entriesString: String = ""
    var entriesCount = 0
    
    val thresholdMillis = 2 * 60 * 1000L
    val maxLines = 20
    
    fun read(): Array<String> {
        entriesCount = 0
        entriesString = sharedPreferences.getString(storeKey, null)
            ?: return emptyArray()
        val split = entriesString.split("\n")
        entriesCount = split.size
        return split.toTypedArray()
    }
    
    fun log( string: String ) {
        read()
        val thisLogMillis = System.currentTimeMillis()
        if( lastLogMillis < thisLogMillis - thresholdMillis ) {
            lastLogMillis = thisLogMillis
            append( SimpleDateFormat( "[yyyy-MM-dd HH:mm:ss,SSS]", Locale.GERMANY ).format( Date( thisLogMillis ) ) )
        }
        append( string )
        
        val editor = sharedPreferences.edit()
        editor.putString( storeKey, entriesString )
        editor.apply()
    }
    
    internal fun append( line: String ) {
        if( entriesCount >= maxLines ) {
            var index = entriesString.indexOf( "\n" )
            while( entriesCount > maxLines && index != -1 ) {
                --entriesCount
                index = entriesString.indexOf( "\n", index+1 )
            }
            entriesString = if( index == -1 ) "" else entriesString.substring( index+1 )
        }
        else {
            ++entriesCount
        }
        entriesString += line
        entriesString += "\n"
    }
}