package de.schlikk.calls

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.CheckBoxPreference
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.SeekBarPreference
import de.schlikk.calls.utils.normalisePhoneNumber


class NumberFilter( private var input: String, private var value: Int, minValue: Int, maxValue: Int, context: Context ) {
    private var importance = false
    private var key = normalisePhoneNumber( input )
    
    private val editText = EditTextPreference( context )
    private val important = CheckBoxPreference( context )
    private val seekBar = SeekBarPreference( context )
    
    var keyChangeListener: ( (NumberFilter, String, String ) -> Boolean )? = null
    var importanceChangeListener: ( (NumberFilter, Boolean, Boolean ) -> Boolean )? = null
    var valueChangeListener: ( (NumberFilter, Int, Int ) -> Boolean )? = null
    var deleteListener: ( (NumberFilter) -> Boolean )? = null
    
    init {
        editText.text = input
        propagateText( input )
    
        important.isChecked = importance
    
        seekBar.value = value
        seekBar.min = minValue
        seekBar.max = maxValue
        
        editText.setOnPreferenceChangeListener( ::adaptKey )
        important.setOnPreferenceChangeListener( ::adaptImportance )
        seekBar.setOnPreferenceChangeListener( ::adaptValue )
    }
    
    fun getInput() = input
    fun getKey() = key
    fun getValue() = value
    fun getImportance() = importance
//    fun setValue( value: Int ) { this.value = value }
    fun preferences() = listOf( editText, important, seekBar )
    
    fun setImportance( value: Boolean ) { importance = value ; important.isChecked = importance }
    

    
    fun store( editor: SharedPreferences.Editor ) {
        editor.putString( editText.key, input )
        editor.putBoolean( important.key, importance )
        editor.putInt( seekBar.key, value )
    }
    fun read( preferences: SharedPreferences ): Boolean {
        if( preferences.contains( editText.key ) ) {
            input = preferences.getString( editText.key, input )!!
            editText.text = input
        }
        if( preferences.contains( important.key ) ) {
            importance = preferences.getBoolean( important.key, importance )
            important.isChecked = importance
        }
        if( preferences.contains( seekBar.key ) ) {
            value = preferences.getInt( seekBar.key, value )
            seekBar.value = value
            return true
        }
        else
            return false
    }
    
    
    
    private fun propagateText( input: String ) {
        key = normalisePhoneNumber( input )
        
        editText.key = "${key}_text"
        editText.title = input
        editText.summary = key
    
        important.key = "${key}_importance"
        important.title = input
    
        seekBar.key = "${key}_volume"
        seekBar.title = input
    }
    
    private fun adaptKey( @Suppress("UNUSED_PARAMETER") preference: Preference, newKey: Any ) =
        if( newKey == "" ) {
            deleteListener?.invoke( this ) ?: true
        }
        else {
            input = newKey as String
            keyChangeListener?.invoke( this, editText.text, input ).also {
                if( it != false )
                    propagateText( input )
                else
                    input = editText.text
            } ?: true
        }
    
    private fun adaptImportance( @Suppress("UNUSED_PARAMETER") preference: Preference, newValue: Any ): Boolean {
        importance = newValue as Boolean
        return importanceChangeListener?.invoke( this, important.isChecked, importance ).also {
            if( it == false )
                importance = important.isChecked
        } ?: true
    }
    
    private fun adaptValue( @Suppress("UNUSED_PARAMETER") preference: Preference, newValue: Any ): Boolean {
        value = newValue as Int
        return valueChangeListener?.invoke( this, seekBar.value, value ).also {
            if( it == false )
                value = seekBar.value
        } ?: true
    }
    
    
    
    fun setTextVisible() {
        editText.isVisible = true
        important.isVisible = false
        seekBar.isVisible = false
    }
    fun setImportanceVisible() {
        editText.isVisible = false
        important.isVisible = true
        seekBar.isVisible = false
    }
    fun setValueVisible() {
        editText.isVisible = false
        important.isVisible = false
        seekBar.isVisible = true
    }
    fun setNothingVisible() {
        editText.isVisible = false
        important.isVisible = false
        seekBar.isVisible = false
    }
    
    
    
    override fun toString(): String {
        return "NumberFilter(input='$input', value=$value, importance=$importance, key='$key')"
    }
}