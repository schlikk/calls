package de.schlikk.calls

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import android.util.Log



class CallReceiver : BroadcastReceiver() {
    override fun onReceive( context: Context, intent: Intent ) {
        Log.d( "CallReceiver", "onReceive" )
        
        val action = intent.action
        Log.d( "CallReceiver", "action: $action")
        
        if( action != "android.intent.action.PHONE_STATE" )
            throw IllegalArgumentException( "CallReceiver#onReceive is not allowed to be called with illegal action \"$action\"" )
        
        val state = intent.getStringExtra( TelephonyManager.EXTRA_STATE )
        Log.d( "CallReceiver", "state: $state")
        val preferenceBasedLogger = if( isPrefLogEnabled(context) ) PreferenceBasedLogger( context, context.getString(R.string.showPrefLogStoreKey) ) else null
        preferenceBasedLogger?.log( "state: $state")

        when( state ) {
            TelephonyManager.EXTRA_STATE_IDLE    -> VolumeManager( context, preferenceBasedLogger=preferenceBasedLogger ).resetPreviousVolume()
            TelephonyManager.EXTRA_STATE_RINGING -> VolumeManager( context, preferenceBasedLogger=preferenceBasedLogger ).overrideVolume( findPreferredVolume( context, intent ) )
            TelephonyManager.EXTRA_STATE_OFFHOOK -> {} // do nothing
            else -> Log.w( "CallReceiver", "Unknown phone state: $state")
        }
    }
    
    private fun isPrefLogEnabled(context: Context): Boolean {
        return context.getSharedPreferences(null, Context.MODE_PRIVATE).getBoolean(
            context.getString(R.string.togglePrefLogStoreKey),
            false
        )
    }
    
    private fun findPreferredVolume(context: Context, intent: Intent ): NumberFilter? {
        val sharedPreferences = context.getSharedPreferences(null, Context.MODE_PRIVATE)
        val all = sharedPreferences.all
        if( all[ context.getString( R.string.toggleNumberFilterStoreKey ) ] == true ) {
            @Suppress("DEPRECATION")
            val number = intent.getStringExtra( TelephonyManager.EXTRA_INCOMING_NUMBER )
            
            Log.d( "CallReceiver", "number := $number")
            if( number != null ) {
                LastCalls( context ).registerNumberInLastCalls( number )
                
                val filter = NumberFilter( number, 0, 0, 0, context )
                if( filter.read( sharedPreferences ) ) {
                    Log.d( "CallReceiver", "numberFilter value for $number is $filter" )
                    return filter
                }
            }
        }
        return null
    }
}
