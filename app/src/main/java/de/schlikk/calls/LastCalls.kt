package de.schlikk.calls

import android.content.Context
import android.util.Log

class LastCalls( val context: ContextWrapper ) {
    constructor( context: Context ): this( ContextWrapper( context ) )
    
    val viewLastCallsListStoreKey = context.getString(R.string.viewLastCallsListStoreKey)
    
    private val lastCallsListDelimiter = "\n"
    private val sharedPreferences = context.getSharedPreferences(null, Context.MODE_PRIVATE)

    
    
    fun toLiteral(string: String ) = ( "\""
        + string
            .replace( "\\", "\\\\" )
            .replace( "\"", "\\\"" )
            .replace( "\n", "\\n" )
        + "\"" )
    
    fun registerNumberInLastCalls( number: String ) {
        Log.d( "LastCalls", "registerNumberInLastCalls( $number )" )
        
        val lastCallsLimit = sharedPreferences.getInt( context.getString(R.string.lastCallsLimitStoreKey), 10 )
        val lastCallsString = sharedPreferences.getString(viewLastCallsListStoreKey, lastCallsListDelimiter ) ?: lastCallsListDelimiter
        Log.d( "LastCalls", "-  " + toLiteral( lastCallsString ) )
        var updated = lastCallsListDelimiter + number + lastCallsString.replace( lastCallsListDelimiter + number + lastCallsListDelimiter, lastCallsListDelimiter )
        Log.d( "LastCalls", "=> " + toLiteral( updated ) )
        if( lastCallsLimit >= 0 ) {
            var count = 0
            var index = 0
            while( index != -1 ) {
                if( ++count >= lastCallsLimit ) {
                    updated = updated.substring( 0, index+1 )
                    Log.d( "LastCalls", "=> " + toLiteral( updated ) )
                    break
                }
                index = updated.indexOf( lastCallsListDelimiter, index+1 )
            }
        }
        sharedPreferences.edit()
            .putString( viewLastCallsListStoreKey, updated )
            .apply()
    }
    
    fun getLastCalls(): Array<String> {
        val string = sharedPreferences.getString(viewLastCallsListStoreKey, lastCallsListDelimiter)
        if( string == null || string == "" || string == lastCallsListDelimiter ) return emptyArray()
        
        return string
            .trim()
            .split(lastCallsListDelimiter)
            .toTypedArray()
    }
    
    fun clear(): Boolean {
        sharedPreferences.edit()
            .putString( viewLastCallsListStoreKey, lastCallsListDelimiter )
            .apply()
        return true
    }
}
