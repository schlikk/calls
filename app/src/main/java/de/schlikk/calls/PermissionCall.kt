package de.schlikk.calls

import android.content.Context

class PermissionCall( val permission: String, val denyDialogMessage: String, var grantAction: (() -> Any?)? = null ) {
    var positiveActionButton: Pair<String, () -> Any?>? = null
    var negativeActionButton: Pair<String, () -> Any?>? = null
    var requestCode = 1
    var neverAskAgainAction: (() -> Any?)? = null
    
    constructor( permission: String, context: Context, denyDialogMessage: Int, grantAction: (() -> Any?)? = null )
        : this( permission, UniFormatter( context ).format( denyDialogMessage, permission ), grantAction )
    {}
    
    
    
    override fun equals( other: Any? ) = this === other
            || ( javaClass == other?.javaClass
                && permission == ( other as PermissionCall ).permission )
    
    override fun hashCode() = permission.hashCode()
    
    override fun toString(): String {
        return "PermissionCall( permission='$permission' )"
    }
}