package de.schlikk.calls

import android.content.Context
import android.text.BidiFormatter
import androidx.annotation.StringRes

class UniFormatter( val context: Context ) {
    val unicodeWrap: ( String ) -> String
    
    init {
        if( android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            val formatter = BidiFormatter.getInstance()
            unicodeWrap = { str -> formatter.unicodeWrap( str ) }
        }
        else {
            unicodeWrap = { str -> str }
        }
    }
    
    
    
    fun wrap( string: String ) = unicodeWrap( string )
    fun format( @StringRes template: Int, vararg strings: String ) = String.format( context.getString( template )
                                                                                    , strings.map( ::wrap ) )
}