package de.schlikk.calls

import androidx.preference.Preference

class TuplePreferenceButton( val preference: Preference ) {
    var key = preference.key
    var state: Int = 0
        get() = field
        set(value) {
            field = value
            preference.title = titles[state]
        }
    var titles = emptyList<String>()
    var onPreferenceChangeListener = fun(_: TuplePreferenceButton, _: Int ) = true
    
    init {
        preference.setOnPreferenceClickListener {
            if( titles.isNotEmpty() ) {
                var newState = state+1
                if( newState >= titles.size ) newState = 0
                if( onPreferenceChangeListener( this, newState ) ) {
                    state = newState
                }
            }
            true
        }
    }
}
