package de.schlikk.calls

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.StringRes

open class ContextWrapper( val context: Context? ) {
    open fun getString( @StringRes resId: Int ): String {
        return context!!.getString( resId )
    }
    
    open fun getSharedPreferences( name: String?, /*@PreferencesMode*/ mode: Int ): SharedPreferences {
        return context!!.getSharedPreferences( name, mode )
    }
}