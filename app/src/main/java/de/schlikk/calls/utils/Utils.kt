package de.schlikk.calls.utils



//    fun normalisePhoneNumber( string: String ) = Regex( "[^0-9+]" ).replace( string, "" )
fun normalisePhoneNumber(string: String ) =
    if( string.length > 1 && string.startsWith( '"' ) && string.endsWith( '"' ) )
        string.substring( 1, string.length-1 )
    else if( string.length > 1 && string.startsWith( '\'' ) && string.endsWith( '\'' ) )
        string.substring( 1, string.length-1 )
    else
        Regex( "[ /()]" ).replace( string, "" )