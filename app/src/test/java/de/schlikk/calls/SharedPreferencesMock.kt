package de.schlikk.calls

import android.content.SharedPreferences

class SharedPreferencesMock : SharedPreferences {
    val map = mutableMapOf<String, Any?>()
    
    override fun getAll(): MutableMap<String, *> = map

    override fun getString(key: String?, defValue: String?): String? = map[key]?.toString()

    override fun getInt(key: String?, defValue: Int): Int = map[key] as Int? ?: defValue
    override fun getLong(key: String?, defValue: Long): Long = map[key] as Long? ?: defValue
    override fun getFloat(key: String?, defValue: Float): Float = map[key] as Float? ?: defValue
    override fun getBoolean(key: String?, defValue: Boolean): Boolean = map[key] as Boolean? ?: defValue
    
    override fun contains(key: String?): Boolean = map.containsKey(key)
    
    
    
    class EditorImpl( val original: MutableMap<String, Any?> ) : SharedPreferences.Editor {
        val copy = original.toMutableMap()
        
        override fun putString(key: String?, value: String?) = this.apply { if( key != null ) copy[key] = value }
        override fun putInt(key: String?, value: Int) = this.apply { if( key != null ) copy[key] = value }
        override fun putLong(key: String?, value: Long) = this.apply { if( key != null ) copy[key] = value }
        override fun putFloat(key: String?, value: Float) = this.apply { if( key != null ) copy[key] = value }
        override fun putBoolean(key: String?, value: Boolean) = this.apply { if( key != null ) copy[key] = value }
    
        override fun remove(key: String?): SharedPreferences.Editor {
            if( key != null ) {
                copy.remove( key )
            }
            return this
        }
    
        override fun clear(): SharedPreferences.Editor {
            copy.clear()
            return this
        }
    
        override fun commit(): Boolean {
            original.keys.retainAll( copy.keys )
            original.putAll( copy )
            return true
        }
    
        override fun apply() {
            commit()
        }
    
        override fun putStringSet(key: String?, values: MutableSet<String>?): SharedPreferences.Editor {
            throw UnsupportedOperationException()
        }
    }

    override fun edit(): SharedPreferences.Editor {
        return EditorImpl( map )
    }
    
    
    
    override fun getStringSet(key: String?, defValues: MutableSet<String>?): MutableSet<String> {
        throw UnsupportedOperationException()
    }

    override fun registerOnSharedPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener?) {
        throw UnsupportedOperationException()
    }

    override fun unregisterOnSharedPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener?) {
        throw UnsupportedOperationException()
    }
}