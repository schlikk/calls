package de.schlikk.calls

import de.schlikk.calls.LastCalls
import org.junit.Assert.assertThat
import org.junit.Test
import org.hamcrest.core.Is.`is` as matches

class LastCallsTest {
    @Test
    fun test() {
        val lastCalls = LastCalls( ContextMock() )
        assertThat(lastCalls.getLastCalls(), matches(emptyArray()))
        lastCalls.registerNumberInLastCalls( "+49 241 1234" )
        assertThat(lastCalls.getLastCalls(), matches(arrayOf("+49 241 1234")))
        lastCalls.registerNumberInLastCalls( "+49 241 5678" )
        assertThat(lastCalls.getLastCalls(), matches(arrayOf("+49 241 5678", "+49 241 1234")))
        lastCalls.registerNumberInLastCalls( "+49 241 5678" )
        assertThat(lastCalls.getLastCalls(), matches(arrayOf("+49 241 5678", "+49 241 1234")))
        lastCalls.registerNumberInLastCalls( "+49 242 9876" )
        assertThat(lastCalls.getLastCalls(), matches(arrayOf("+49 242 9876", "+49 241 5678", "+49 241 1234")))
        lastCalls.registerNumberInLastCalls( "+49 241 1234" )
        assertThat(lastCalls.getLastCalls(), matches(arrayOf("+49 241 1234", "+49 242 9876", "+49 241 5678")))
    }
}