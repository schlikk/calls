package de.schlikk.calls.utils

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Test

class UtilsTest {
    @Test
    fun `test normalise literals`() {
        assertThat( normalisePhoneNumber( "'+46 158 3456'" ), `is`( "+46 158 3456" ) )
        assertThat( normalisePhoneNumber( "\"+46 158 3456\"" ), `is`( "+46 158 3456" ) )
    }
    @Test
    fun `test normalise real phoneNumbers`() {
        assertThat( normalisePhoneNumber( "+46 158 3456" ), `is`( "+461583456" ) )
        assertThat( normalisePhoneNumber( "+46 (0) 158 3456" ), `is`( "+4601583456" ) )
        assertThat( normalisePhoneNumber( "(158) 3456" ), `is`( "1583456" ) )
        assertThat( normalisePhoneNumber( "0158/3456" ), `is`( "01583456" ) )
    }
    @Test
    fun `test strange input`() {
        assertThat( normalisePhoneNumber( "" ), `is`( "" ) )
        assertThat( normalisePhoneNumber( "'+46 158 3456" ), `is`( "'+461583456" ) )
        assertThat( normalisePhoneNumber( "abc + def" ), `is`( "abc+def" ) )
    }
}