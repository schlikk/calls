package de.schlikk.calls

import android.content.SharedPreferences
import de.schlikk.calls.ContextWrapper

class ContextMock : ContextWrapper( null ) {
    override fun getString( resId: Int ): String {
        return resId.toString()
    }
    
    
    
    override fun getSharedPreferences( name: String?, mode: Int ): SharedPreferences {
        return SharedPreferencesMock()
    }
}